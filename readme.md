# DisplayMapper Trigger

DisplayMapper Trigger is a simple method for syncing two displaymapper units within a couple of seconds of one another.

## Requirements

  - 2x arduino pro micros.
  - 2x USB to micro USB
  - 1x push button
 
## Wiring

- connect the pushbutton to digital pin 3 on both arduinos.
- connect power and ground to push button.

## License
WTFPL